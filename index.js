console.log ("Hello world!");
// enclose with quotation mark string data.

console. log ("Hello world!");
// case sensitive

console.
log
(
    "Hello, everyone!"
)

// ; delimeter
//  We use delimeter to end our dode.

// [COMMENTS]
// - single line comments
// Shortcut: ctrl + /

// I'm a single line comment

/* Multi-line comment */
/* 
    I'm
    a
    multi-line
    comment
*/
/* shortcut (VS code):
    shift + alt + A
*/

// Syntax and Statement

// Statements in programming are instructions that we tell to computer to perform.
// Syntax in programming are the set of rules that describes how statements must be considered.

// Variables
/* 

    It is used to contain data

    -Syntax in declaring variables
    - let/const variableName
*/


let myVariable = "Hello";
                // assignment operator (=);
console.log (myVariable);

let temperature = 82;

// console.log (hello); -- will no result to not defined error.

/* 
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative or descriptive of the value being stored.
        5. Never name a variable starting with numbers.
        6. Refrain from using space in declaring a variable.
*/

// Strings
let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer"
console.log(product);

// Numbers
let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// Reassigning varialbe value
// Syntax
    // variableName = newValue; 

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

// interest = 4.89; --- const variables cannot be changed.

const pi = 3.14;

// Reassigning - a variable already have a value and we-reassign a new one.
// Initialize - its our first saving of a value.

let supplier;
supplier = "John Smith Tradings";
            // Initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell"
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
// const let = "hello";
// console.log(let); --- will not work with reserved keyword

// [SECTION] Data Types

// Strings
// Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.
// Strings in JavaScript is enclosed with single ('') or double ("") quote.

let country = 'Philippines';
let province = "Hello Manila";

console.log(province + ', ' + country);

// We use + symbol to concatenate data / value.

let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

// Escapae Characters (\)
//  "\n" refers to creating a new line or set the text to next line.

console.log("line1\nline2");

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early."
console.log(message);

message = 'John\'s employee went home early';
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers / float / fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10
console.log(planetDistance);

console.log ("John's grade last quarter is " + grade);

// Arrays
// It stores multiple values with similar data type

let grades = [98.7, 95.4, 90.2, 94.6];
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Another special kind of data type that is used to mimic real world objects or items.

// Syntax:

/* 
    let/const objectName = {
        propertyName (key): property value,
        propertyName: value
    }
*/

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["0912 345 6789", "8123 7444"],
    address: {
        houseNumber: "345",
        city: "Manila",
    }
};

console.log(person);

const myGrades = {
    firstGrading: 98.7,
    secondGrading: 95.4,
    thirdGrading: 90.2,
    fourthGrading: 94.6,
};

console.log(myGrades);

let stringValue = "string";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
// myGrades as object

// 'type of' operator
// We use 'type of' operator to retrieve / know the data type.

console.log(typeof stringValue); 
// output: string
console.log(typeof numberValue); 
// output: number
console.log(typeof booleanValue);
// output: boolean
console.log(typeof waterBills);
// output: object
console.log(typeof myGrades);
// output: object

// Constant Objects and Arrays
// We cant reassign the value of the variable

const anime = ["Boruto", "One Piece", "Code Geas", "Monster", "Dan Maci", "Demon Slayer", "AOT", "Fairy Tale"];

// [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
// index - is the position of the element starting zero.

anime [0] = "Naruto";
console.log(anime);

// Null
// It's used to intentionally express the absence of a value.
let spouse = null;
console.log(spouse);

// Undefined
// Represents the state of a variable that has been declared but without an assigned value
let fullName = undefined;
console.log(fullName);